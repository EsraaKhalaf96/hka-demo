import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../pages/shared-module/shared.module';
import { HeaderComponent } from './header-component/header-component.component';
import { FooterComponent } from './footer-component/footer-component.component';


@NgModule({
  imports: [
    BrowserModule,
    SharedModule,
  ],
  declarations: [HeaderComponent, FooterComponent],
  exports: [
    HeaderComponent,
    FooterComponent
  ]
})
export class CoreModule { }
