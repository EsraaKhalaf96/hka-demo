import { Component, OnInit , HostListener} from '@angular/core';


@Component({
  selector: 'header-component',
  templateUrl: './header-component.component.html'
})
export class HeaderComponent implements OnInit {
  topPosToShowFixed = 100;
  navFixed: boolean = false;
  constructor() { }

  ngOnInit() {
   
  }
  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    console.log('[scroll]', scrollPosition);
  
    if (scrollPosition >= this.topPosToShowFixed) {
      this.navFixed = true;
    } else {
      this.navFixed = false;
    }
  }
}
