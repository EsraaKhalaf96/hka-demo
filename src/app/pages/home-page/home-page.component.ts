import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeInLeft ,fadeInRight} from 'ng-animate';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  animations: [
    trigger('fadeInLeft', [transition('* => *', useAnimation(fadeInLeft, {
      // Set the duration to 5seconds and delay to 2seconds
      params: { timing: 3, delay: 3 }
    }))]),trigger('fadeInRight', [transition('* => *', useAnimation(fadeInRight, {
      // Set the duration to 5seconds and delay to 2seconds
      params: { timing: 3, delay: 3 }
    }))])
  ]
})
export class HomePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
   
  }
  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
 
}
