import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { FormsModule }          from '@angular/forms';
import { HttpClientModule }  from '@angular/common/http';

import { RouterModule, Routes } from '@angular/router';

import { CoreModule } from './core/core.module';
import { LoaderComponent } from './loader/loader.component';
import { ClickOutsideModule } from 'ng-click-outside';

import { HomePageComponent } from './pages/home-page/home-page.component';
import {  AboutPageComponent } from './pages/about-page/about-page.component';
import { AppComponent } from './app.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { WorkPageComponent } from './pages/work-page/work-page.component';
import { ServicesPageComponent } from './pages/services-page/services-page.component';
import { ProjectDetailsPageComponent } from './pages/project-details-page/project-details-page.component';
import { BrandingComponent } from './pages/work-page/branding/branding.component';
import { WebsiteComponent } from './pages/work-page/website/website.component';
import { AppWorkComponent } from './pages/work-page/app-work/app-work.component';
import { VectorArtComponent } from './pages/work-page/vector-art/vector-art.component';
import { MotionComponent } from './pages/work-page/motion/motion.component';
import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';

const appRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomePageComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: 'work', component: WorkPageComponent ,children:[
    {path:'branding' , component:BrandingComponent},
    {path:'website' , component:WebsiteComponent},
    {path:'app' , component:AppWorkComponent},
    {path:'vector-art' , component:VectorArtComponent},
    {path:'motion' , component:MotionComponent},
  ] },
  { path: 'services', component: ServicesPageComponent },
  {path:'project-details' , component:ProjectDetailsPageComponent}


];

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    HomePageComponent,
    AboutPageComponent,
    ContactPageComponent,
    WorkPageComponent,
    ServicesPageComponent,
    ProjectDetailsPageComponent,
    BrandingComponent,
    WebsiteComponent,
    AppWorkComponent,
    VectorArtComponent,
    MotionComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    ClickOutsideModule,
    AnimateOnScrollModule.forRoot(),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
