// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
// import { HomePageComponent } from './pages/home-page/home-page.component';
// import {  AboutPageComponent } from './pages/about-page/about-page.component';

// const routes: Routes = [
//   { path: '', pathMatch: 'full', redirectTo: '/home' },
//   { path: 'home', component: HomePageComponent },

//   // {
//   //   path: 'home',
//   //   loadChildren: () => import('./pages/home-page/home-page.module').then(mod => mod.HomePageModule)
//   // },
//   {
//     path: 'work',
//     loadChildren: () => import('./pages/work-page/work-page.module').then(mod => mod.WorkPageModule)
//   },
//   {
//     path: 'services',
//     loadChildren: () => import('./pages/services-page/services-page.module').then(mod => mod.ServicesPageModule)
//   }
//   ,
//   {
//     path: 'about',
//     loadChildren: () => import('./pages/about-page/about-page.module').then(mod => mod.AboutPageModule)
//   }
//   ,
//   {
//     path: 'contact',
//     loadChildren: () => import('./pages/contact-page/contact-page.module').then(mod => mod.ContactPageModule)
//   },
//   {
//     path: 'project-details',
//     loadChildren: () => import('./pages/project-details-page/project-details-page.module').then(mod => mod.ProjectDetailsPageModule)
//   }

// ]

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule],
//   declarations:[HomePageComponent , AboutPageComponent]
// })
// export class AppRoutingModule { }
